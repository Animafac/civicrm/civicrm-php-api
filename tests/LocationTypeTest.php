<?php
/**
 * LocationTypeTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\LocationType;

/**
 * Tests for the LocationType class.
 */
class LocationTypeTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = (object) [
            'id' => 42
        ];
        $this->locationType = new LocationType(42);
    }

    /**
     * Test the getDefaultType() function.
     *
     * @return void
     */
    public function testGetDefaultType()
    {
        $this->assertInstanceOf(LocationType::class, LocationType::getDefaultType());
    }
}
