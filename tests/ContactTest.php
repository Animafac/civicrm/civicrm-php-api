<?php
/**
 * ContactTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Address;
use CivicrmApi\Contact;
use CivicrmApi\Country;
use CivicrmApi\Email;
use CivicrmApi\LocationType;
use CivicrmApi\Phone;
use CivicrmApi\Relationship;
use CivicrmApi\RelationshipType;
use CivicrmApi\Tag;
use CivicrmApi\User;
use CivicrmApi\StateProvince;
use CivicrmApi\Website;
use CivicrmApi\WebsiteType;

use stdClass;

/**
 * Tests for the Contact class.
 */
class ContactTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = (object) [
            'foo' => 'bar',
            'id' => 42,
            'contact_id' => 42
        ];
        $this->contact = new Contact(42);
        $this->civicrmApi->result = new stdClass();
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertEquals('bar', $this->contact->get('foo'));
        $this->assertEquals(42, $this->contact->get('id'));
        $this->assertNull($this->contact->get('unknownproperty'));
    }

    /**
     * Test the getWebsites() function.
     *
     * @return void
     */
    public function testGetWebsites()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach ($this->contact->getWebsites() as $website) {
            $this->assertInstanceOf(Website::class, $website);
        }
    }

    /**
     * Test the getWebsite() function.
     *
     * @return void
     */
    public function testGetWebsite()
    {
        $this->civicrmApi->values = [
            (object) ['value' => 'goodwebsite', 'key' => 42],
            (object) ['value' => 'badwebsite', 'key' => 666]
        ];
        $this->civicrmApi->result = (object) [
            'url' => 'http://example.com',
            'id' => 42
        ];

        $goodWebsite = $this->contact->getWebsite(new WebsiteType('goodwebsite'));
        $this->assertEquals('http://example.com', $goodWebsite->get('url'));

        $badWebsite = $this->contact->getWebsite(new WebsiteType('badwebsite'));
        $this->assertNull($badWebsite);
    }

    /**
     * Test the getWebsite() function with an unkown website type.
     *
     * @return void
     * @expectedException Exception
     */
    public function testGetWebsiteWithError()
    {
        $this->contact->getWebsite(new WebsiteType('unknownwebsite'));
    }

    /**
     * Test the setWebsite() function.
     *
     * @return void
     */
    public function testSetWebsite()
    {
        $this->civicrmApi->values = [
            (object) ['value' => 'badwebsite', 'key' => 666]
        ];

        $this->assertNull($this->contact->setWebsite(new WebsiteType('badwebsite'), 'http://example.com'));
    }

    /**
     * Test the getOrganizations() function.
     *
     * @return void
     */
    public function testGetOrganizations()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->civicrmApi->result = (object) ['id' => 42];
        foreach ($this->contact->getOrganizations(new RelationshipType(42)) as $organization) {
            $this->assertInstanceOf(Contact::class, $organization);
        }
    }

    /**
     * Test the getMembers() function.
     *
     * @return void
     */
    public function testGetMembers()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->civicrmApi->result = (object) ['id' => 42];
        foreach ($this->contact->getMembers(new RelationshipType(42)) as $member) {
            $this->assertInstanceOf(Contact::class, $member);
        }
    }

    /**
     * Test the getParentRelationship() function.
     *
     * @return void
     */
    public function testGetParentRelationship()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(
            Relationship::class,
            $this->contact->getParentRelationship(
                new Contact(42),
                new RelationshipType(42)
            )
        );
    }

    /**
     * Test the getTags() function.
     *
     * @return void
     */
    public function testGetTags()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach ($this->contact->getTags() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the getTags() function.
     *
     * @return void
     */
    public function testHasTag()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertTrue($this->contact->hasTag(new Tag(42)));
    }

    /**
     * Test the getTags() function.
     *
     * @return void
     */
    public function testHasTagWithFalse()
    {
        $this->civicrmApi->result = (object) ['id' => 666];
        $this->assertFalse($this->contact->hasTag(new Tag(666)));
    }

    /**
     * Test the addTag() function.
     *
     * @return void
     */
    public function testAddTag()
    {
        $this->civicrmApi->result = (object) ['id' => 666];
        $this->assertNull($this->contact->addTag(new Tag(666)));
    }

    /**
     * Test the removeTag() function.
     *
     * @return void
     */
    public function testRemoveTag()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertNull($this->contact->removeTag(new Tag(42)));
    }

    /**
     * Test the getUser() function.
     *
     * @return void
     */
    public function testGetUser()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(User::class, $this->contact->getUser());
    }

    /**
     * Test the getUser() function.
     *
     * @return void
     */
    public function testGetUserWithError()
    {
        $this->civicrmApi->result = (object) [
            'id' => 666,
            'contact_id' => 666
        ];
        $contact = new Contact(666);
        $this->assertNull($contact->getUser());
    }

    /**
     * Test the getByEmail() function.
     *
     * @return void
     */
    public function testGetByEmail()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(Contact::class, Contact::getByEmail('contact@example.com'));
    }

    /**
     * Test the getByUserId() function.
     *
     * @return void
     */
    public function testGetByUserId()
    {
        $this->assertInstanceOf(Contact::class, Contact::getByUserId(42));
    }

    /**
     * Test the getById() function.
     *
     * @return void
     */
    public function testGetById()
    {
        $this->assertInstanceOf(Contact::class, Contact::getById(42));
    }

    /**
     * Test the canEdit() function.
     *
     * @return void
     */
    public function testCanEdit()
    {
        $this->civicrmApi->result = (object) [
            'id' => 42,
            'is_permission_a_b' => true
        ];
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->assertTrue($this->contact->canEdit(new Contact(42)));
    }

    /**
     * Test the canEdit() function.
     *
     * @return void
     */
    public function testCanEditWithFalse()
    {
        $this->civicrmApi->result = (object) [
            'id' => 42
        ];
        $this->assertFalse($this->contact->canEdit(new Contact(42)));
    }

    /**
     * Test the isMember() function.
     *
     * @return void
     */
    public function testIsMember()
    {
        $this->civicrmApi->result = (object) ['id' => 42,];
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->assertTrue($this->contact->isMember(new Contact(42), new RelationshipType(42)));
    }

    /**
     * Test the isMember() function.
     *
     * @return void
     */
    public function testIsMemberWithFalse()
    {
        $this->civicrmApi->result = (object) [
            'id' => 42
        ];
        $this->assertFalse($this->contact->isMember(new Contact(42), new RelationshipType(42)));
    }

    /**
     * Test the isComember() function.
     *
     * @return void
     */
    public function testIsComember()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->assertTrue(
            $this->contact->isComember(
                new Contact(42),
                new RelationshipType(42)
            )
        );
    }

    /**
     * Test the isComember() function.
     *
     * @return void
     */
    public function testIsComemberWithFalse()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertFalse(
            $this->contact->isComember(
                new Contact(42),
                new RelationshipType(42)
            )
        );
    }

    /**
     * Test the hasAdmin() function.
     *
     * @return void
     */
    public function testHasAdmin()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        $this->assertTrue($this->contact->hasAdmin(new Contact(42)));
    }

    /**
     * Test the hasAdmin() function.
     *
     * @return void
     */
    public function testHasAdminWithFalse()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertFalse($this->contact->hasAdmin(new Contact(42)));
    }

    /**
     * Test the addRelationship() function.
     *
     * @return void
     */
    public function testAddRelationship()
    {
        $this->civicrmApi->result = (object) [
            'id' => 42,
            'contact_id' => 42
        ];
        $this->assertInstanceOf(
            Relationship::class,
            $this->contact->addRelationship(
                new Contact(42),
                new RelationshipType(42)
            )
        );
    }

    /**
     * Test the addRelationship() function.
     *
     * @return void
     */
    public function testAddRelationshipWithNewRelationship()
    {
        $this->civicrmApi->result = (object) [
            'id' => 666,
            'contact_id' => 42
        ];
        $this->assertInstanceOf(
            Relationship::class,
            $this->contact->addRelationship(
                new Contact(42),
                new RelationshipType(666)
            )
        );
    }

    /**
     * Test the setAddress() function.
     *
     * @return void
     */
    public function testSetAddress()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(
            Address::class,
            $this->contact->setAddress(
                'street_address',
                'supplemental_address',
                'city',
                'postal_code',
                new LocationType(42)
            )
        );
    }

    /**
     * Test the setAddress() function.
     *
     * @return void
     */
    public function testSetAddressWithCountry()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(
            Address::class,
            $this->contact->setAddress(
                'street_address',
                'supplemental_address',
                'city',
                'postal_code',
                new LocationType(42),
                new Country(42),
                new StateProvince(42)
            )
        );
    }

    /**
     * Test the setEmail() function.
     *
     * @return void
     */
    public function testSetEmail()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(
            Email::class,
            $this->contact->setEmail(
                'email_address',
                new LocationType(42)
            )
        );
    }

    /**
     * Test the setEmail() function with an empty e-mail address.
     *
     * @return void
     */
    public function testSetEmailWithEmptyAddress()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertNull(
            $this->contact->setEmail(
                '',
                new LocationType(42)
            )
        );
    }

    /**
     * Test the setPhone() function.
     *
     * @return void
     */
    public function testSetPhone()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertInstanceOf(
            Phone::class,
            $this->contact->setPhone(
                'phone_number',
                new LocationType(42)
            )
        );
    }

    /**
     * Test the setPhone() function with an empty phone number.
     *
     * @return void
     */
    public function testSetPhoneWithEmptyNumber()
    {
        $this->civicrmApi->result = (object) ['id' => 42];
        $this->assertNull(
            $this->contact->setPhone(
                '',
                new LocationType(42)
            )
        );
    }

    /**
     * Test the getCount() function.
     *
     * @return void
     */
    public function testGetCount()
    {
        $this->civicrmApi->lastResult = 2;
        $this->assertEquals(2, Contact::getCount(['foo' => 'bar']));
    }
}
