<?php
/**
 * WebsiteTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Website;
use CivicrmApi\WebsiteType;

/**
 * Tests for the Website class.
 */
class WebsiteTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = (object) [
            'id' => 42,
            'website_type_id' => 42
        ];
        $this->website = new Website(42);
    }

    /**
     * Test the getType() function.
     *
     * @return void
     */
    public function testGetType()
    {
        $this->civicrmApi->values = [
            (object) ['key' => 42, 'value' => 'website_type_name']
        ];
        $this->assertInstanceOf(WebsiteType::class, $this->website->getType());
    }

    /**
     * Test the getType() function.
     *
     * @return void
     */
    public function testGetTypeWithNullResult()
    {
        $this->assertNull($this->website->getType());
    }
}
