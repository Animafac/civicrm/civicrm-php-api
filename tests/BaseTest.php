<?php
/**
 * BaseTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use PHPUnit\Framework\TestCase;
use Mockery;
use civicrm_api3;
use stdClass;

/**
 * Class used to create mocks common to every test class.
 */
abstract class BaseTest extends TestCase
{

    /**
     * civicrm_api3 instance.
     *
     * @var civicrm_api3
     */
    protected $civicrmApi;

    /**
     * Api instance.
     * @var Api
     */
    protected $api;

    /**
     * Add necessary properties to the mock civicrm_api3 object.
     *
     * @param civicrm_api3 $civicrmApi Mock civicrm_api3 object
     */
    protected function addCivicrmObjects(civicrm_api3 $civicrmApi)
    {
        $civicrmApi->Address =
            $civicrmApi->Contact =
            $civicrmApi->Country =
            $civicrmApi->Email =
            $civicrmApi->EntityTag =
            $civicrmApi->LocationType =
            $civicrmApi->Note =
            $civicrmApi->Phone =
            $civicrmApi->Relationship =
            $civicrmApi->RelationshipType =
            $civicrmApi->StateProvince =
            $civicrmApi->Tag =
            $civicrmApi->User =
            $civicrmApi->Website = new civicrm_api3();

        return $civicrmApi;
    }

    /**
     * Create mock classes.
     */
    protected function setUp()
    {
        $this->civicrmApiMock = Mockery::mock('overload:civicrm_api3')
            ->shouldReceive('getsingle')
                ->with(['foo' => 'bar'])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with([])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['id' => 666])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => 666])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => 42, 'website_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => 42, 'website_type_id' => 666])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['entity_table' => 'civicrm_contact', 'entity_id' => 42, 'tag_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['entity_table' => 'civicrm_contact', 'entity_id' => 42, 'tag_id' => 666])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['email' => 'contact@example.com', 'contact_type' => null])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42, 'relationship_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42, 'relationship_type_id' => 666])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42])
                ->andReturn(false)
            ->shouldReceive('getsingle')
                ->with(['is_default' => true])
                ->andReturn(true)
            ->shouldReceive('getsingle')
                ->with(['iso_code' => 'fr'])
                ->andReturn(true)

            ->shouldReceive('getoptions')
                ->with(['field' => 'goodtype'])
                ->andReturn(true)
            ->shouldReceive('getoptions')
                ->with(['field' => 'unknowntype'])
                ->andReturn(false)
            ->shouldReceive('getoptions')
                ->with(['field' => 'website_type_id'])
                ->andReturn(true)
            ->shouldReceive('getoptions')
                ->with(['field' => 'gender_id'])
                ->andReturn(true)

            ->shouldReceive('get')
                ->with(['foo' => 'bar'])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with([])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['wrong_constraint'])
                ->andReturn(false)
            ->shouldReceive('get')
                ->with(['parent_id' => 42, 'is_tagset' => false, 'options' => []])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['is_tagset' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id' => 42])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => 42, 'is_active' => true, 'relationship_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_b' => 42, 'is_active' => true, 'relationship_type_id' => 42])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['entity_table' => 'civicrm_contact', 'entity_id' => 42])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42, 'is_active' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42, 'relationship_type_id' => 42, 'is_active' => true])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_b' => 42, 'is_active' => true, 'is_permission_a_b' => 2])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['contact_id_b' => 42, 'is_active' => true, 'is_permission_a_b' => 1])
                ->andReturn(true)
            ->shouldReceive('get')
                ->with(['country_id' => 42, 'options' => ['limit' => 100, 'sort' => 'abbreviation']])
                ->andReturn(true)

            ->shouldReceive('getcount')
                ->with(['foo' => 'bar'])
                ->andReturn(true)
            ->shouldReceive('getcount')
                ->with(['wrong_constraint'])
                ->andReturn(false)

            ->shouldReceive('create')
                ->with(['foo' => 'bar'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['wrong_constraint'])
                ->andReturn(false)
            ->shouldReceive('create')
                ->with(['contact_id' => '42', 'website_type_id' => 666, 'url' => 'http://example.com'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['entity_table' => 'civicrm_contact', 'entity_id' => 42, 'tag_id' => 666])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['id' => 42, 'contact_id' => 42, 'is_active' => true,
                    'is_permission_a_b' => 2, 'description' => ''])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id_a' => 42, 'contact_id_b' => 42, 'relationship_type_id' => 666,
                    'is_active' => true, 'is_permission_a_b' => 2, 'description' => ''])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42,
                    'street_address' => 'street_address', 'supplemental_address_1' => 'supplemental_address',
                    'city' => 'city', 'postal_code' => 'postal_code', 'state_province_id' => ''])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42,
                    'street_address' => 'street_address', 'supplemental_address_1' => 'supplemental_address',
                    'city' => 'city', 'postal_code' => 'postal_code',
                    'country_id' => 42, 'state_province_id' => 42])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42, 'email' => 'email_address'])
                ->andReturn(true)
            ->shouldReceive('create')
                ->with(['contact_id' => 42, 'is_primary' => true, 'location_type_id' => 42, 'phone' => 'phone_number'])
                ->andReturn(true)

            ->shouldReceive('delete')
                ->with(['foo' => 'bar'])
                ->andReturn(true)
            ->shouldReceive('delete')
                ->with(['wrong_constraint'])
                ->andReturn(false)
            ->shouldReceive('delete')
                ->with(['id' => 42])
                ->andReturn(true)

            ->shouldReceive('errorMsg');

        $this->civicrmApi = $this->addCivicrmObjects(new civicrm_api3());

        $this->civicrmApi->values = [];
        $this->civicrmApi->result = new stdClass();

        Api::$path = 'foo';
        $this->api = new Api($this->civicrmApi);
        Api::setInstance($this->api);
    }

    /**
     * Destroy mock classes.
     *
     * @return void
     */
    protected function tearDown()
    {
        Mockery::close();
    }
}
