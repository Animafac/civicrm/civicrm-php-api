<?php
/**
 * LocationType class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM location types.
 */
class LocationType extends ApiObject
{
    /**
     * Get the default location type.
     *
     * @return LocationType
     */
    public static function getDefaultType()
    {
        return self::getSingle(['is_default' => true]);
    }
}
