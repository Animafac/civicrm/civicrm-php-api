<?php
/**
 * Website class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM websites.
 */
class Website extends ApiObject
{
    /**
     * Get the type of this website.
     * @return WebsiteType
     */
    public function getType()
    {
        foreach ($this->api->getOptions('Website', 'website_type_id') as $type) {
            if ($type->key == $this->get('website_type_id')) {
                return new WebsiteType($type->value);
            }
        }
    }
}
