<?php
/**
 * StateProvince class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM provinces.
 */
class StateProvince extends ApiObject
{

    /**
     * Get all provinces in this country.
     *
     * @param  Country $country Country ISO code
     * @return StateProvince[]
     */
    public static function getAllFromCountry(Country $country)
    {
        return self::getAll(
            [
                'country_id' => $country->get('id'),
                'options' => [
                    'limit' => 100,
                    'sort' => 'abbreviation'
                ]
            ]
        );
    }
}
