<?php
/**
 * Tag class.
 */

namespace CivicrmApi;

/**
 * Manage CiviCRM tags.
 */
class Tag extends ApiObject
{
    /**
     * Get all tags with the specified parent.
     *
     * @param string $sort Sort key
     *
     * @return Tag[]
     */
    public function getChildren($sort = null)
    {
        $options = [];
        if ($sort) {
            $options['sort'] = $sort;
        }

        return self::getAll([
            'parent_id' => $this->get('id'),
            'is_tagset' => false,
            'options' => $options
        ]);
    }

    /**
     * Get all tagsets with no parent.
     * @return Tag[]
     */
    public static function getAllTagsets()
    {
        return self::getAll(['is_tagset' => true]);
    }

    /**
     * Convert tag to string.
     *
     * @return string Tag name
     */
    public function __toString()
    {
        return $this->get('name');
    }
}
